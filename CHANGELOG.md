# Nitroforce Changelog

This is a nitroforce utility chanelog to keep track of changes commited to the repository.


## 0.2.4 - nitrogenez authored 2023-11-03
### Added
  + Two more dependencies:
    - [clap](https://crates.io/crates/clap) and [colorized](https://crates.io/crates/colorized)
  + New argument parsing system, thanking clap

### Fixed
  + Refactor code a little
  + Changed license to GNU General Public License v3-or-later (see [LICENSE.md](LICENSE.md))


## 0.2.5 - nitrogenez authored 2023-12-03
### Added
  + Finnaly added colorized output

### Fixed
  + Refactored code
  + Updated output style
  + pinlist.txt was moved to `doc/`
  + Separated installation instructions for adb


## 0.2.6 - nitrogenez authored 2023-12-03
### Added
  + Fully-featured random password support
