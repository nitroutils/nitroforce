<img src=".gitlab/assets/banner.png" alt="Icon" width=100%>

## Description
Software made for enthusiasts

Picks numbers in range from 0 to 9 or from PIN list and passes generated combination into Android smartphone through adb.

> **Attention!**  
> This software was developed for educational purposes ONLY.  
Author does not take any responsibility for any unfair and/or malicious usage.


## Building
  + Install Rust and Cargo (e.g via rustup)
  + Build project: `cargo build --release`


## Adb Installation
For Linux see [LinuxAdbInstallation](doc/LinuxAdbInstallation.md)  
For MacOSX see [MacOSXAdbInstallation](doc/MacOSXAdbInstallation.md)


## Quick Guide
+ Enable USB-Debugging on target Android phone
+ **Linux/MacOS X**
  - Clone or download repository: `git clone https://gitlab.com/nitroutils/nitroforce`
  - Connect target device to your machine via USB
  - Goto `nitroforce` directory: `cd /path/to/nitroforce`
  - Build project


## Example
```bash
git clone https://gitlab.com/nitroutils/nitroforce
cd ~/nitroforce
cargo build --release
./target/release/nitroforce
```

## License
**This software is licensed under GNU General Public License v3-or-later.**
See [LICENSE](LICENSE.md) for further details.