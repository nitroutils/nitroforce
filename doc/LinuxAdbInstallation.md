## Install via package manager

> **Note**  
> If you didn't find instruction for your specific distribution, then you need to find a way to install adb by yourself.

### Arch Linux (and Arch-based distributions)
  + `pacman -S android-tools`

### Debian (and Debian-based distributions e.g Ubuntu)
  + `apt-get install android-tools-adb`

### OpenSUSE
  + `zypper install android-tools`

### Fedora
  + `dnf install android-tools`
