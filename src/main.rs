// Internal modules
mod npin;
mod passwd;


// use crate::passwd::Passwd;
use crate::{npin::NPin, passwd::Passwd};

// External modules
use clap::Parser;
use colorized::{colorize_this, Colors};

use std::process::exit;



#[derive(Default, Parser, Debug)]
#[clap(author = "nitrogenez", version, about = "Simple Android PIN bruteforcer written in Rust")]
struct Args {
    #[clap(short, long)]
    method: String,

    #[clap(short, long)]
    length: Option<usize>,

    #[clap(short, long)]
    file: Option<String>,

    #[clap(short, long)]
    verbose: Option<usize>,
}


fn main() {
    let args: Args = Args::parse();

    println!("| {}", colorize_this("Attention", Colors::BrightYellowFg));
    println!("| This software was developed for educational purposes {}", colorize_this("ONLY", Colors::BrightYellowFg));
    println!("| Author does not take any responsibility for any unfair and/or malicious usage");
    println!();

    let adb_server = mozdevice::Host { ..Default::default() };
    let connected: Vec<mozdevice::DeviceInfo> = adb_server.devices().unwrap();
    let connected_amount: usize = connected.len();


    if connected_amount <= 0 {
        println!("{}: No devices", colorize_this("Error", Colors::BrightRedFg));
        exit(1);
    }

    let active_device_info = connected.get(0).unwrap();
    let active_device = mozdevice::Device::new(
        adb_server,
        active_device_info.serial.to_owned(),
        mozdevice::AndroidStorageInput::Auto
    ).unwrap();


    println!("{}: {connected_amount}", colorize_this("Connected", Colors::BrightGreenFg));
    println!(
        "{}: {} ({})\n",
        colorize_this("Active", Colors::BrightGreenFg),
        active_device_info.info["device"],
        active_device_info.info["model"]
    );

    match args.method.as_str() {
        "pin"       => NPin::random(&active_device, args.length.unwrap()),
        "pinlist"   => NPin::from_file(&args.file.unwrap(), &active_device),

        "rpasswd"   => Passwd::random(&active_device, args.length.unwrap()),
        &_          => println!("Nothing to do.")
    }
}