use colorized::Colors;
use colorized::colorize_this;
use mozdevice::Device;

use rand::thread_rng;
use rand::Rng;

use std::thread::sleep;
use std::time::Duration;


pub struct Passwd {}


impl Passwd {

    fn get_random_char(charset: &[u8]) -> char {
        let index = thread_rng().gen_range(0..charset.len());
        charset[index] as char
    }

    // Generate & push random  password
    pub fn random(device: &Device, length: usize) {
        println!(
            "{}: Random password generation is unstable and not effective at all",
            colorize_this("Warning", Colors::BrightYellowFg)
        );

        // Character list
        let charset: &[u8] = &concat!(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "abcdefghijklmnopqrstuvwxyz",
            "0123456789",
            "#*"
        ).to_string().into_bytes();

        let mut att: i32  = 0;
        let mut ret_limit: i16 = 5;


        // Wake device
        device.execute_host_shell_command("input keyevent 82").expect("Failed to execute shell command");
        sleep(Duration::from_millis(100));
        device.execute_host_shell_command("input swipe 408 1210 508 85").expect("Failed to execute shell command");
        sleep(Duration::from_millis(100));


        // Generate 9999 passwords and push them
        for ret in 1..=9999 {
            let passwd: String = (0..length).map(|_| Passwd::get_random_char(&charset)).collect();

            if att >= 3 {
                println!(
                    "{}: Limit of 3 retries exceeded, max retries: {ret_limit}",
                    colorize_this("Warning", Colors::BrightYellowFg)
                );
                ret_limit = 1;
            }

            println!(
                "{}: {att}, {}: {ret}, {}: {passwd}",
                colorize_this("Attempt", Colors::BrightCyanFg),
                colorize_this("Retry", Colors::BrightCyanFg),
                colorize_this("Password", Colors::BrightCyanFg)
            );

            device.execute_host_shell_command(&format!("input text {:?}", passwd))
                .expect("Failed to execute shell command");
            device.execute_host_shell_command("input keyevent 66")
                .expect("Failed to execute shell command");

            if ret % ret_limit == 0 {
                println!(
                    "{}: Limit of {ret_limit} retries exceeded, waiting 30 seconds...",
                    colorize_this("Warning", Colors::BrightYellowFg)
                );
                att += 1;

                sleep(Duration::from_millis(80));
                device.execute_host_shell_command("input keyevent 66").expect("Failed to execute shell command");
                sleep(Duration::from_secs(30));

                // Wake device again
                device.execute_host_shell_command("input keyevent 82").expect("Failed to execute shell command");
                sleep(Duration::from_millis(100));
                device.execute_host_shell_command("input swipe 408 1210 508 85").expect("Failed to execute shell command");
                sleep(Duration::from_millis(100));
            }
        }
    }
}